# bash scripts

- check.sh - shows currently logged in user, current directory and date.
- omv-installer.sh - install OpenMediaVault
- twitch.sh - record twitch livestream - bash twitch.sh "twitch_channel"
- update.sh - update, upgrade and autoremove
