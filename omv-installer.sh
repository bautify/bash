#!/bin/bash

echo""
echo""
echo"Update and Upgrade system..."
sudo apt update
sudo apt upgrade
echo""
echo""
echo"Installing OpenMediaVault..."
wget -O - https://raw.githubusercontent.com/OpenMediaVault-Plugin-Developers/installScript/master/install | sudo bash
echo""
echo""
echo""
echo "This is your ip ..."
hostname -I
echo""
echo""
echo"Restarting System..."
sudo reboot
