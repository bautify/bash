#!/bin/bash

echo ""
echo "Update the apt packages index...."
sudo apt update &>/dev/null
echo""
echo "Upgrade and install the latest versions of packages..."
sudo apt upgrade -y &>/dev/null
echo""
echo "Removing those dependencies that were installed with applications and are no longer used by anything else on the system "
echo""
sudo apt autoremove -y &>/dev/null
echo "Done!"
echo""
