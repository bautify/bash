#!/bin/bash

user=$(whoami)
date=$(date)
whereami=$(pwd)

echo "Your are currently logged in as $user and you are in the directory $whereami. Also today is: $date"
